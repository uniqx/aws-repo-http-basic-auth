## About

This Project contains instructions and code for hosting a HTTP-Basic-Auth
protected F-Droid repository on Amazon AWS.

This solution relies on following Services: S3 for hosting files,
CloudFront as CDN, Lambda@Edge for implementing HTTP-Basic-Auth.

## Limitations
Following this guide the S3 bucket is still accessible via unauthenticated HTTP. So the S3 butcket name should be kept secret.

## Setup Guide

This guide assumes you are familiar with F-Droid and AWS.

1. Go to AWS console: https://console.aws.amazon.com
1. Setup source repo bucket
    1. Create a AWS S3 bucket. (eg. call it: my-fdroid-repo)
    1. Deploy F-Droid repo to S3 bucket (eg. using _fdroid server update_)
    1. Make sure the files in the bucket are publically accessible.
       (eg. by acessing my-fdroid-repo./fdroid/repo/index.xml with a
       web-browser.)
    1. Upload HTTP-Basic Auth credentials file.
       (eg. demo\_credentials.auth-credentials.json to the root directory
       of your bucket.) Make sure to set _'Manage public permissions'_
       to _'Grant public read access to this objects(s)'_
       when configuring permissions. (You may use
       [provi](https://gitlab.com/uniqx/provi) to genereate a
       HTTP-Basic-Auth credentials file.)

1. Deploying AWS Lamba@Edge Function (Policy)
    1. Go to IAM > Policy and create a new one.
    1. Select JSON and paste this policy definition:

            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": [
                            "logs:CreateLogGroup",
                            "logs:CreateLogStream",
                            "logs:PutLogEvents"
                        ],
                        "Resource": [
                            "arn:aws:logs:*:*:*"
                        ]
                    }
                ]
            }

    1. Assing a policy name and save. (eg. my-lambda-edge-policy)

1. Deploying AWS Lamba@Edge Function (Role)
    1. Go to IAM > Roles and create a new one.
    1. Select Lambda click next.
    1. Select your previously created policy.
    1. Assing a role name and save. (eg. my-lambda-edge-role)
    1. Go to Trust relationship tab when viewing details of this role click: Edit trust relationship
    1. replace it with this JSON snippet:

            {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": [
                      "lambda.amazonaws.com",
                      "edgelambda.amazonaws.com"
                    ]
                  },
                  "Action": "sts:AssumeRole"
                }
              ]
            }

1. Deploying AWS Lamba@Edge Function (CloudFront)
    1. Go to CloudFront > Distributions and create a new one.
    1. Select Web and cofigure it like this:
        * Origin Domain Name (eg.): my-fdroid-repo.s3.amazonaws.com
        * Origin ID (eg.): S3-my-fdroid-repo
        * Restrict Bucket Access: No
        * Viewer Protocol Policy (eg.): Redirect HTTP to HTTPS
    1. click Create Distribution
    1. You can see the domain name of the new distribution now. If you did
       configure it use your own domain (recommended) it should resemble:
       _d1izinodfqwe3z.cloudfront.net_

1. Deploying AWS Lamba@Edge Function (Lambda)
    1. Go to Lambda > Functions.
    1. Make sure you are connected to this AWS location: __N. Virginia__
       (drop-down menu on to navigation bar.)
    1. Click on the Create function button.
    1. Selet Author from scratch:
        * Name: (eg. my-lambda-edge-http-basic-auth)
        * Runtime: Node.js 6.10
        * Role: Chose an existing role
        * Existing role: previously created role (eg. my-lambda-edge-role)
    1. Click create function
    1. Paste the content of __http-basic-auth.js__ into text editor (replace the entire content of index.js).
    1. Update the params variable in the script to reflect your S3 setup (eg.):

            // please configure this to reflect your setup
            const params = {
                // S3 bucket name, which contains the HTTP-Basic Auth credentials json-file
                Bucket: "my-fdroid-repo",
                // Path to the HTTP-Basic Auth credentials json-file
                Key: "fdroid/repo/demo_credentials.http-credentials.json"
            };

    1. Click the orange save button at the top right.
    1. Click actions > publish new version
    1. Supply a description (eg. inital upload) and click publish
    1. Select CloudFront from the list of the available tirggers in the designer and configure it:
        * Distribution ID: ID of the previously created distribution. (eg. EBWAZ39223DEO)
        * Cache Behavior: *
        * CloudFront Event: Viewer Request
        * Enable trigger and replicate: checked
    1. Click add button
    1. Click save button again

1. Test accessing it (eg. curl 'https://user:pass@djwg5h4ye45j.cloudfront.net/fdroid/repo/index.xml')

## Update Credentials

To update existing credentials simply update the content of the
JSON-Credentials file in your S3 bucket.
(eg. fdroid/repo/demo\_credentials.http-credentials.json)
For generating credential files you can use
[provi](https://gitlab.com/uniqx/provi) CLI tool.
