/**
 * HTTP-Basic Auth implementation for AWS Lambda@Edge
 */

'use strict';

var AWS = require('aws-sdk');
var crypto = require('crypto');
var util = require('util');

// please configure this to reflect your setup
const params = {
        // S3 bucket name, which contains the HTTP-Basic Auth credentials json-file
        Bucket: "Bucketname goes here",
        // Path to the HTTP-Basic Auth credentials json-file 
        Key: "path/to/file.http-credentials.json"
    };

exports.handler = (event, context, callback) => {
    
    // Get request and request headers
    const request = event.Records[0].cf.request;
    const headers = request.headers;
    
    if (request.uri == params.Key) {
        callback(null, {status: 404, body: 'File not found.'});
        return;
    }
    
    new AWS.S3().getObject(params, function(err, data) {
        if (err) {
            //console.log("granting access: error occured: " + err);
            callback(null, {status: 500, body: "error reading '" + params.Key + "': " + err});
            return;
        }
        
        var credentials = [];
        try {
            credentials = JSON.parse(data.Body.toString());
        } catch (err) {
            callback(null, {status: 500, body: "granting access: could not parse '" + params.Key + "'\n" + err + "\n" + data.Body.toString()});
            return;
        }

        // check for basic authentication header
        if (typeof headers.authorization == 'undefined') {
            const response = {
                status: '401',
                statusDescription: 'Unauthorized',
                body: 'Unauthorized',
                headers: {
                    'www-authenticate': [{key: 'WWW-Authenticate', value:'Basic'}]
                },
            };
            callback(null, response);
            return;
        }
        
        const authVals = Buffer.from(headers.authorization[0].value.substring(6), 'base64').toString().trim().split(':');
        
        if (authVals.length != 2) {
            const response = {
                status: '400',
                statusDescription: 'Bad Request',
                body: 'Bad Request: HTTP Authorization Header malformed.\n' + util.inspect(authVals) + '\n' + headers.authorization[0].value
            };
            callback(null, response);
            return;
        }
    
        // parse basic authentication header
        const checkCredentials = function(credentials, user, pass) {
            for (var i=0; i<credentials.length; i++) {
                if (credentials[i][0] == user) {
                    return String(credentials[i][1]).toLowerCase() == pass;
                }
            }
            return false;
        };
        
        const salt = crypto.createHash('sha256').update('f-droid.org').digest('hex');
        const passhash = crypto.createHash('sha256').update(salt + authVals[1]).digest('hex');
        
        if (checkCredentials(credentials, authVals[0], passhash) == true) {
            // Continue request processing if authentication passed
            callback(null, request);
        } else {
            const response = {
                status: '403',
                statusDescription: 'Forbidden',
                body: '403 - Forbidden - user credentials incorrect'
            };
            callback(null, response);
            return;
        }
    });
};
